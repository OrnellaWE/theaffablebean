<%-- 
    Document   : panier
    Created on : 5 mars 2020, 11:41:02
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="demo.categorie,demo.produit,demo.panier,demo.cartServlet,demo.affServlet,demo.catServlet,demo.tdataAdapterCategorie"%>
<%@page import="java.util.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cart</title>
        
           <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/lip/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#clear").click(function(){
   // $("td").remove(".text-right");
   $(".text-right").empty();
  });
}
        
                );
</script>
        
    </head>
    <body>
        
         <jsp:include page="gabarit.jsp"/> 
         <br/>
             <section class="jumbotron text-center">
           <div class="container">
        <h3 class="jumbotron-heading">YOUR SHOPPING CART</h3>
     </div>
                <br/>
                
</section>
        
                
        <%
             HttpSession s = request.getSession();

List<panier> pn=(List<panier>)s.getAttribute("cart");
      
   int taille=0;         // for (produit p : pn.listproduits){
       for(int i=0;i<pn.size();i++){
           taille+=pn.get(i).getQte();
       }     

                                 %>
      <br/>
        <div class="container mb-4">
               <h5><i style="color: crimson;">Your cart contains : <%=taille%>  item(s)</i></h5>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                        
      <table class="table table-striped">
          <thead>
              <tr>
              <th scope="col">Product Image</th>
              <th scope="col">Product Name</th>
              <th scope="col" class="text-center">Quantity</th>
              <th scope="col" class="text-center">Price</th>
              </tr>  
              
          </thead>
                <%
             
int qte=1;
       
                if (pn!= null) {
                 for(panier p:pn){ 

                        %>  
          <tbody>
              <tr>
                   <td class="text-right"><img src="<%=p.getleprod().getImage()%>" alt="image absente" width="150px" height="150px"/></td>    
                  <td class="text-right"><%=p.getleprod().getNom()%></td>
                  <td class="text-right"><!--<input name="qte" type="text" class="form-control" value="--><%=p.getQte()%></td>
                            <td class="text-right"> $ <%=p.grandTotal()%></td>
                            
                    
                            <td class="text-right"><button style="color:white;" class="btn btn-sm btn-danger"><a href="cartServlet?action=plus&idt=<%=p.getleprod().getId()%>">+</a></button></td>
                            <td class="text-right"><button style="color:white;" class="btn btn-sm btn-danger"><a href="cartServlet?action=minus&idt=<%=p.getleprod().getId()%>">-</a></button></td>
                    <td class="text-right"><button style="color:white;height:40px;width:150px;float:right;text-align:center" id="clear" class="btn btn-lg btn-block btn btn-success text-uppercase"><a href="cartServlet?action=remove&idt=<%=p.getleprod().getId()%>">remove</a></button></td>   
              </tr> 
                      
          </tbody>  
  
  <%  
      

} 
}

  %>               
         
      </table> 
  
       </div>
        </div>

     <div class="col mb-2">
            <div class="row">
                <div class="col-sm-12  col-md-6">
                    <button class="btn btn-block btn-light" style="width:200px;height:60px"><a href="categorie.jsp" style="color:black;">Continue Shopping</a></button>
                </div>
                    <div class="col-sm-12 col-md-6 text-right">
                                   <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_cart">
    <input type="hidden" name="upload" value="1">
    <input type="hidden" name="business" value="merchant78@gmail.com">
    
    <% for(panier bs: pn){ %>
    
    <input type="hidden" name="item_name_<%=qte%>" value="<%=bs.getleprod().getNom() %>" />
    <input type="hidden" name="item_number_<%=qte%>" value="<%=bs.getleprod().getId() %>" />
    <input type="hidden" name="amount_<%=qte%>" value="<%=bs.getleprod().getPrix() %>" />
    <input type="hidden" name="quantity_<%=qte%>" value="<%=bs.getQte()%>" />
    
    <% qte++; } %>
    
     <input type="hidden" name="currency_code" value="USD" />
    <input type="hidden" name="return" value="https://www.teccart.qc.ca/" />
    <input type="hidden" name="cancel_return" value="https://www.bnc.ca/" />
   
    <input type="submit" class="btn btn-lg btn-block btn-success text-uppercase"  value="Checkout" />
    
   
                    </form>

                    </div>
                
                
            </div>
     </div>
    </div>
        </div>
  
    </body>
</html>
