<%-- 
    Document   : categorie
    Created on : 10 sept. 2019, 14:57:58
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="demo.categorie,demo.produit,demo.affServlet,demo.cartServlet,demo.panier,demo.catServlet,java.util.*,demo.tdataAdapterCategorie" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
        <title>Categories</title>
        <style>
            
            #customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 70%;
  float:right;
  height: 350px;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
  
}


#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
     #add{
  background-color: Crimson;
  border: none;
  color: white;
  text-align: center;
  display: inline-block;
  font-size: 13px;
  cursor: pointer;
text-align: center;
width:100px;
height:40px;
border-radius: 5px 5px 5px 5px;
            }          
          .but{
      
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-decoration: none;
  display: inline-block;
  font-size: 20px;
  margin: 4px 2px;
  cursor: pointer;
text-align: center;
width:300px;
margin-top:40px;
            }                  
                   
 section #leftblock{
                width:20%;
                height:100%;
                display: inline-block;
                             }
                             
             section #rightblock{
                 float:right;
                width:80%;
                height:100%;
                display: inline-block;
             
                           }
                           
                                                     
            #rightblock ul{
                width:100%;
                        
            }
            
           #leftblock  #categorie{
                width:100%;
                height: 100%;
                
            }   
             footer{  
                background-color:#4CAF50;
              
            }
            #guess{
              float:right;  
            }
            ul {
          text-align: center;    
            list-style-type: none;
            
            }
            .inf{
                font-family: cambria;
                font-size: 1.5em;
                color:goldenrod;
                font-weight:bold;
                font-style:italic;
            }
           
            
        
            .but:visited{
                background: crimson;
            }
                .but:focus{
                background: crimson;
            }
            .but:hover{
                background: crimson;
            }
            .but:active{
                background: crimson;
            }
            
                       </style>
        
                <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
                       <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    </head>
    <body>
        <%
             HttpSession s = request.getSession();

List<panier> pn=(List<panier>)s.getAttribute("cart");
      
   int taille=0;   
   if(pn!=null){
       for(int i=0;i<pn.size();i++){
            taille+=pn.get(i).getQte();
       }
   }
   
                                 %>
                                 <div id="cadre">
                                 <h5 style="text-align: right"><i style="color: black">Your cart contains : <%=taille%>  item(s)</i></h5>
                                 </div>
    <jsp:include page="gabarit.jsp"/> 
       
     
        <section>
            <br/>
        <div id="leftblock">
                  <% 
                      
                      categorie ctg = (categorie)session.getAttribute("mycate");
                  
         for(produit p : ctg.listproduct)
            { %> 

            <div id="categorie">
               
                <a class="act" href="catServlet?id=<%=p.getId()%>"><button class="but "><%=p.getNom()%></button> </a>


                     
        </div> 
          <%}%>       
        </div>
            
        <div id="rightblock">
     <% 
   categorie cx= (categorie)session.getAttribute("cate");
    %> 
    
<ul id="guess">
    
    <li><img src="<%=cx.getProduit().getImage()%>" alt="image absente" width="250px" height="250px"/></li>
    
       <li class="inf"><%=cx.getProduit().getNom()%></li><br/>
     <li class="inf">$ <%=cx.getProduit().getPrix()%></li><br/>
<li>

    <a href="cartServlet?action=add&idt=<%=cx.getProduit().getId()%>"><button  id="add" >Ajouter</button></a></td>   
</li>
</ul>
 

        </div>
        </section>
<br/><br/>
        <footer><h5>Copyright 2019 - All rights reserved</h5></footer>
          
           </body>
</html>
