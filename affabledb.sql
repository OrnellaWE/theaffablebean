-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 01 Mai 2020 à 01:24
-- Version du serveur :  5.6.20
-- Version de PHP :  5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `affabledb`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `idcategorie` varchar(50) NOT NULL,
  `nomcat` varchar(45) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`idcategorie`, `nomcat`, `description`, `image`) VALUES
('cat01', 'Dairy', 'This category concerns dairy products and their derivatives ', './img/oeufs.jpg'),
('cat02', 'Meats', 'This category concerns various meats and their derivatives', './img/viandesfraiches1-250x181.jpg'),
('cat03', 'Bakery', 'This category is about bakeries:bread,pies,muffins,...', './img/bakery.jpg'),
('cat04', 'Fruits & vegetables', 'This category is about fruits and vegetables', './img/fruits-legumes.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `idproduit` varchar(50) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `prix` double NOT NULL,
  `idcategorie` varchar(45) NOT NULL,
  `quantite` int(11) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`idproduit`, `nom`, `prix`, `idcategorie`, `quantite`, `image`) VALUES
('1', 'Grape bread ', 5, 'cat03', 34, './images/brioche.jpg'),
('10', 'pork belly', 24, 'cat02', 26, './images/poitrine.jpg'),
('11', 'Strawberry Quebon milk', 5, 'cat01', 15, './images/quebon.jpg'),
('12', 'chicken drumstick', 18, 'cat02', 24, './images/pilon.jpg'),
('13', 'onion', 14, 'cat04', 55, './images/oignons.jpg'),
('14', 'Swiss cheese', 9, 'cat01', 17, './images/swiss.jpg'),
('15', 'french baguette', 3, 'cat03', 17, './images/baguette.png'),
('16', 'lemon pie', 7, 'cat03', 5, './images/tarte.jpg'),
('2', 'Banana', 4, 'cat04', 4, './images/bananes.jpg'),
('3', 'Soy milk', 6, 'cat01', 8, './images/laitsoja.jpg'),
('4', 'Eggs', 6, 'cat01', 18, './images/oeufspoule.jpg'),
('5', 'Beef ribs', 45, 'cat02', 11, './images/coteboeuf.jpg'),
('6', 'Tiramisu', 12, 'cat03', 5, './images/tiramisu.jpg'),
('7', 'Apple', 8, 'cat04', 30, './images/pommes.jpg'),
('8', 'smoked turkey ham', 6, 'cat02', 25, './images/jambon.jpg'),
('9', 'potatoes', 30, 'cat04', 25, './images/patates.jpg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
 ADD PRIMARY KEY (`idcategorie`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
 ADD PRIMARY KEY (`idproduit`), ADD KEY `fk_categorie` (`idcategorie`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
ADD CONSTRAINT `fk_categorie` FOREIGN KEY (`idcategorie`) REFERENCES `categorie` (`idcategorie`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
