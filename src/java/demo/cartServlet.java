/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
public class cartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      
    
         String url = "jdbc:mysql://localhost:3306/affabledb";
    String user ="root";
    String pw = "";
      
                    tdataAdapterCategorie adpter = new tdataAdapterCategorie(url, user, pw);//connection bd
       ArrayList<produit> cart= new ArrayList<produit>();
       List<panier> paniers=new ArrayList<panier>();
          int id = Integer.parseInt(request.getParameter("idt"));       
        String action=request.getParameter("action");
       // cart=adpter.infospanier(id);
       produit p=adpter.getbyId(id);
       
        HttpSession s = request.getSession();
     
   
if (p != null) {
    
            int etat = 0;
            if (s.getAttribute("cart") != null) {
              paniers = (List<panier>) s.getAttribute("cart");           
              
                for (panier ps : paniers) {   
                    int plus;
                    //INCREMENTATION EN AJOUTANT A NOUVEAU LE MEME PRODUIT 
                 if(ps.getleprod().getId()==id) {
           ps.setQte(ps.getQte());
               plus=adpter.updateplus(id);  
               etat=1;
                   
                }
                
                }
                 if (etat == 0) {
                     paniers.add(new panier(p,1));
               
                s.setAttribute("cart", paniers);
               
            
            }  
                
                 
              
                     if(action != null){
                          for(int i=0;i<paniers.size();i++){
                          //BOUTON AJOUTER QTE
          if(action.equals("plus")){
                            
                if(paniers.get(i).getleprod().getId()==id) {
                  paniers.get(i).setQte(paniers.get(i).getQte()+1);   
             int plus=adpter.updateplus(id);
               
            }
                 }
          
          //BOUTON DIMINUER QTE
            if(action.equals("minus")){
                            
                if(paniers.get(i).getleprod().getId()==id) {
                  paniers.get(i).setQte(paniers.get(i).getQte()-1);
             int  moins=adpter.updateminus(id);
            }
                 }
            
                if(action.equals("remove")){
                            
                if(paniers.get(i).getleprod().getId()==id) {
                  paniers.remove(paniers.get(i));
            }
                 }
            
            
                      }                          
           
            }
               
            }
            
  
                            
            if(s.getAttribute("cart") == null){
         
                paniers.add(new panier(p,1));
                s.setAttribute("cart", paniers);
            }
            
}
      
        RequestDispatcher vue = request.getRequestDispatcher("/panier.jsp"); 
             vue.forward(request, response); 
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

