 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

/**
 *
 * @author Admin
 */
public class tdataAdapterCategorie {
     private Connection conn;
    private PreparedStatement allproductInfo,productInfo,updatemoins,updateplus;
    private ResultSet result;
    private String url,user,pw;
    //private String id;
    
    
    public tdataAdapterCategorie(String url,String user,String pw)
    {
        this.url = url;
        this.user = user;
        this.pw = pw;
         try
        {
                      
           Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            
        }catch (SQLException e){
        //Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, e);
   
        }catch (Exception se){
                  //   
                     }
        
        }
               
   public ArrayList<produit> getpan(int idp)
    {
       ArrayList<produit> listprod=new  ArrayList<produit>();
        
              
        
        try 
        {      
             Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            allproductInfo = conn.prepareStatement("select * from produits where idproduit= ?;");
            allproductInfo.setInt(1,idp);
            result=this.allproductInfo.executeQuery();
            
            while(result.next())
            {
            
                listprod.add(new produit(result.getInt("idproduit"),result.getString("nom"),result.getDouble("prix"),result.getInt("quantite"),result.getString("image")));
            }
          
             
        }
        catch(SQLException ex)
        {
            Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception se){
                 
                     }
        
         return listprod;
    }
    
    
    public ArrayList<produit> getAllproductInfo(String idcat)
    {
       ArrayList<produit> listprod=new  ArrayList<produit>();
        
              
        
        try 
        {      
             Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            allproductInfo = conn.prepareStatement("select * from produits where idcategorie= ?;");
            allproductInfo.setString(1,idcat);
            result=this.allproductInfo.executeQuery();
            
            while(result.next())
            {
            
                listprod.add(new produit(result.getInt("idproduit"),result.getString("nom"),result.getDouble("prix"),result.getInt("quantite"),result.getString("image")));
            }
          
             
        }
        catch(SQLException ex)
        {
            Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception se){
                 
                     }
        
         return listprod;
    }
    
      public ArrayList<categorie> getcate()
    {
       ArrayList<categorie> listcat=new  ArrayList<categorie>();
        
              
        
        try 
        {      
             Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            allproductInfo = conn.prepareStatement("select * from categorie;");
            result=this.allproductInfo.executeQuery();
            
            while(result.next())
            {
            
                listcat.add(new categorie(result.getString("idcategorie"),result.getString("nomcat"),result.getString("description"),result.getString("image")));
            }
          
             
        }
        catch(SQLException ex)
        {
            Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception se){
                 
                     }
        
         return listcat;
    }
    
     public ArrayList<produit> infospanier(int idprod)
    {
       ArrayList<produit> listprod=new  ArrayList<produit>();
        
              
        
        try 
        {      
             Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            allproductInfo = conn.prepareStatement("select * from produits where idproduit= ?;");
            allproductInfo.setInt(1,idprod);
            result=this.allproductInfo.executeQuery();
            
            while(result.next())
            {
            
                listprod.add(new produit(result.getInt("idproduit"),result.getString("nom"),result.getDouble("prix"),result.getInt("quantite"),result.getString("image")));
            }
          
             
        }
        catch(SQLException ex)
        {
            Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception se){
                 
                     }
        
         return listprod;
    }
   
   
    public produit getbyId(int id)
    {
       int idproduit= 0;
               String Nom="";
               double Prix=0.0;
                String categorie="";

                String pic="";
                int qte=0;
       // ArrayList<produit> aprod = new ArrayList<produit>();
       //produit aprod = new produit(idproduit,Nom,Prix,categorie,pic);
           
        
        try 
        {      
             Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
           productInfo = conn.prepareStatement("select * from produits where idproduit= ?;");
            productInfo.setInt(1,id);
            result=this.productInfo.executeQuery();
            
            while(result.next())
            {
              
               idproduit= result.getInt("idproduit");
                Nom= result.getString("nom");
                Prix= result.getDouble("prix");
                
                qte=result.getInt("quantite");
                 pic=result.getString("image");
                
                 
            }
          
              
        }
        catch(SQLException ex)
        {
            Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }catch (Exception se){
                  //   
                     }
          produit aprod = new produit(idproduit,Nom,Prix,qte,pic);
          
         return aprod;
    }
    
     public int updateplus(int id){
           int rows=0;
        try 
        {      
             Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            updateplus = conn.prepareStatement("update produits set quantite=quantite-1  where idproduit= ?;");
           
           this.updateplus.setInt(1,id);
           
           rows = this.updateplus.executeUpdate();
            
        }
        catch (SQLException ex)
        {
            Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
         catch (Exception se){
                 
                     }
        
          
        return rows ;
        
    }
     
     
      public int updateminus(int id){
           int rows=0;
           
            int idproduit= 0;
               String Nom="";
               double Prix=0.0;
           
                String pic="";
                int qte=0;
                 String msg="";
         // produit p=new produit(id);
        try 
        {      
             Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            updatemoins = conn.prepareStatement("update produits set quantite=quantite+1  where idproduit= ?;");

      
          this.updatemoins.setInt(1,id);
           
           rows = this.updatemoins.executeUpdate();
           
        }
        catch (SQLException ex)
        {
            Logger.getLogger(tdataAdapterCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
         catch (Exception se){
                 
                     }
        
          
        return rows; 
        
    }
    
    
}
    

